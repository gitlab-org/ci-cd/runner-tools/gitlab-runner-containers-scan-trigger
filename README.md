# GitLab Runner Containers Scan Trigger

:construction: This project has been obsoleted by
https://gitlab.com/gitlab-com/gl-security/threatmanagement/vulnerability-management/vulnerability-management-internal/vulnmapper

This project implements [Configure gitlab-runner related docker images to be scanned for
vulnerabilities](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/29387). It triggers container vulnerability scans
for all the relevant docker images produced as part of the [gitlab-runner](https://gitlab.com/gitlab-org/gitlab-runner)
project, and reports the results in such a way that it is obvious to runner developers when action is required on their
part to resolve significant vulnerabilities.

This is implemented in the following phases:
1. Generate a list of the URIs of the containers to scan for a single project. This includes determining which tag to
   use for each container.
2. Trigger the vulnerabilities scan pipelines, wait for the scan pipelines to finish, and capture the resulting
   artifacts in this project.
3. Report scan results in actionable manner. Implementation of this is TBD.

All three steps are implemented in the Go script included in this project. The actual container vulnerability scans are
implemented [here](https://gitlab.com/gitlab-com/gl-security/appsec/container-scanners). The script can be run locally
(on a developer workstation) like so.
```sh
> ./container-scanner <project-id> container-list.yml --pipeline-trigger-token ***** --pipeline-api-token *****
```
for example
```sh
> ./container-scanner gitlab-org/gitlab-runner container-list.yml --pipeline-trigger-token ***** --pipeline-api-token *****
```
The pipeline trigger and API tokens can be obtained from the shared 1Password vault.

The list of containers to interpolate and scan are specified in `container-list.yml`, which is a map of `project-ids` to
containers. Interpolation and scanning is done one project at a time to facilitate reporting of the scan results, which
for e.g. may involve creating an issue against the corresponding project. _Note that reporting is not yet implemented_.

## Container Tag Interpolation

Currently 3 container tag interpolation strategies are supported:
- `@release`: replace the `@release` token with the project's latest semantic (not necessarily chronological) release
  version. For example `gitlab-org/gitlab-runner:@release` which interpolates to `gitlab-org/gitlab-runner:v15.10.1`.
- `@last`: replace the `@last` token with the image's latest/last/newest tag. This requires listing all tags for that
  image and selecting the last one. For example `gitlab-org/gitlab-runner/go-fips:@last` which interpolates to
  `gitlab-org/gitlab-runner/go-fips:1.19`. 
- `default`: no interpolation. For example `gitlab-org/gitlab-runner:bleeding`.

More strategies can be added if necessary.

# Scanning Pipeline 

The scanning pipeline runs as a `matrix` of `project-id`s. This is done primarily to facilitate downstream reporting as
described above, but also to keep individual downstream scan jobs to a reasonable size. Within a project, all containers
are scanned in a single job. In the [downstream
pipeline](https://gitlab.com/gitlab-com/gl-security/appsec/container-scanners) a single pipeline scans all containers
for a single `project-id`.
