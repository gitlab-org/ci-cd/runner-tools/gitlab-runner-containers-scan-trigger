module gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-runner-containers-scan-trigger

go 1.20

require (
	github.com/alexflint/go-arg v1.4.3
	github.com/xanzy/go-gitlab v0.81.0
	golang.org/x/exp v0.0.0-20230321023759-10a507213a29
	golang.org/x/mod v0.9.0
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/alexflint/go-scalar v1.1.0 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hashicorp/go-retryablehttp v0.7.2 // indirect
	golang.org/x/net v0.8.0 // indirect
	golang.org/x/oauth2 v0.6.0 // indirect
	golang.org/x/time v0.3.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.29.0 // indirect
)
