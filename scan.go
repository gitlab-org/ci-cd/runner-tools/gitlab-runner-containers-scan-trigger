package main

import (
	"fmt"
	"io"
	"os"
	"strings"
	"time"

	"github.com/xanzy/go-gitlab"
	"golang.org/x/exp/slices"
)

var (
	pipelineCompletionStatuses = []string{"success", "failed", "canceled", "skipped"}
	pipelineJobs               = []string{"transform:t2g", "transform:a2g"}
	jobPollInterval            = 30 * time.Second
)

const (
	scanArtifactName       = "gl-container-scanning-report.json"
	containerScanProjectID = "gitlab-com/gl-security/appsec/container-scanners"
)

func (a *App) scanContainers(projectID string, containers []string) error {
	pipeline, err := a.triggerContainerScanPipeline(projectID, containers)
	if err != nil {
		return err
	}
	err = a.waitForPipeline(pipeline)
	if err != nil {
		return err
	}
	jobID, err := a.getPipelineJob(pipeline.ID)
	if err != nil {
		return err
	}
	return a.downloadScanResult(jobID)
}

func (a *App) triggerContainerScanPipeline(projectID string, containers []string) (*gitlab.Pipeline, error) {
	pipeline, _, err := a.api.PipelineTriggers.RunPipelineTrigger(containerScanProjectID, &gitlab.RunPipelineTriggerOptions{
		Ref:       gitlab.String("master"),
		Token:     gitlab.String(a.ContainerScanTriggerToken),
		Variables: map[string]string{"IMAGES": strings.Join(containers, ",")},
	})
	if err != nil {
		return nil, fmt.Errorf("failed to run container scan pipeline for project %q: %w", projectID, err)
	}

	fmt.Printf("started container scan pipeline %d for project %q\n", pipeline.ID, projectID)
	return pipeline, nil
}

func (a *App) waitForPipeline(pipeline *gitlab.Pipeline) error {
	start := time.Now()
	var err error
	fmt.Printf("waiting %v for pipeline %d to finish ", a.Timeout, pipeline.ID)
	for !slices.Contains(pipelineCompletionStatuses, pipeline.Status) && time.Since(start) < a.Timeout {
		time.Sleep(jobPollInterval)
		fmt.Printf(".")

		pipeline, _, err = a.api.Pipelines.GetPipeline(containerScanProjectID, pipeline.ID)
		if err != nil {
			return fmt.Errorf("failed to get staus for pipeline %d: %w", pipeline.ID, err)
		}
	}
	fmt.Println("")

	if !slices.Contains(pipelineCompletionStatuses, pipeline.Status) {
		return fmt.Errorf("timed out waiting for pipeline %d to finish. final status: %q", pipeline.ID, pipeline.Status)
	}
	if pipeline.Status != "success" {
		return fmt.Errorf("pipeline %d did not finish successfully: %q", pipeline.ID, pipeline.Status)
	}

	fmt.Printf("pipeline %d finish successfully\n", pipeline.ID)
	return nil
}

func (a *App) getPipelineJob(pipelineID int) (int, error) {
	jobID := -1

	jobs, _, err := a.api.Jobs.ListPipelineJobs(containerScanProjectID, pipelineID, nil)
	if err != nil {
		return jobID, fmt.Errorf("failed to list jobs for pipeline %d: %w", pipelineID, err)
	}

	for i := range jobs {
		if !slices.Contains(pipelineJobs, jobs[i].Name) {
			continue
		}
		jobID = jobs[i].ID
		break
	}
	if jobID == -1 {
		return jobID, fmt.Errorf("failed to find transform job for pipeline %d", pipelineID)
	}
	return jobID, nil
}

func (a *App) downloadScanResult(jobID int) error {
	fmt.Printf("downloading artifact for job %d\n", jobID)
	artifact, response, err := a.api.Jobs.DownloadSingleArtifactsFile(containerScanProjectID, jobID, scanArtifactName)
	if err != nil {
		return fmt.Errorf("failed to download job artifact %s: %w", scanArtifactName, err)
	}
	defer response.Body.Close()
	f, err := os.Create(scanArtifactName)
	if err != nil {
		return fmt.Errorf("failed to open file %q for writing: %w", scanArtifactName, err)
	}
	defer f.Close()
	_, err = io.Copy(f, artifact)
	if err != nil {
		return fmt.Errorf("failed to write artifact to file: %w", err)
	}

	fmt.Printf("successfully downloaded artifact for job %d\n", jobID)
	return nil
}
