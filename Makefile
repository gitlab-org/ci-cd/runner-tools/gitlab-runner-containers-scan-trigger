APP ?= container-scanner

$(APP): *.go Makefile
	go build -o $(APP) .

.PHONY: lint
lint:
	golangci-lint run

.PHONY: scan-containers
scan-containers: $(APP)
	./$(APP) $(PROJECT) container-list.yml \
		--pipeline-api-token $(CONTAINER_SCAN_PROJECT_API_TOKEN) --pipeline-trigger-token $(CONTAINER_SCAN_PIPELINE_TRIGGER_TOKEN)

.PHONY: clean
clean:
	rm -fr $(APP)
