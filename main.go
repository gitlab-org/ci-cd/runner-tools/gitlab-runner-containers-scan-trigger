package main

import (
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/alexflint/go-arg"
	"github.com/xanzy/go-gitlab"
)

type (
	App struct {
		ProjectID                 string        `arg:"required, positional" help:"The project whose containers to scan."`
		ContainersListFile        string        `arg:"required, positional" help:"Path to file containing list of containers to scan."`
		ContainerScanAPIToken     string        `arg:"required,-a,--pipeline-api-token,env:CONTAINER_SCAN_PROJECT_API_TOKEN" help:"The Pipeline project API token"`
		ContainerScanTriggerToken string        `arg:"required,-p,--pipeline-trigger-token,env:CONTAINER_SCAN_PIPELINE_TRIGER_TOKEN" help:"Pipeline trigger token."`
		Release                   string        `arg:"-r,--release,env:RELEASE" help:"Specify a release version with which to replace @release tokens in the containers list."`
		Timeout                   time.Duration `arg:"-t,--timeout" default:"30m" help:"Time to wait for scan pipeline to complete"`
		ListOnly                  bool          `arg:"-l,--list-only" help:"List the containers to be scanned, but do not triger the scan."`

		api *gitlab.Client
	}
)

func main() {
	app := App{}

	if err := app.Init(); err != nil {
		log.Fatalln(err)
	}

	if err := app.Run(); err != nil {
		log.Fatalln(err)
	}
}

func (a *App) Init() error {
	arg.MustParse(a)

	var err error

	a.api, err = gitlab.NewClient(a.ContainerScanAPIToken)
	if err != nil {
		return fmt.Errorf("failed to create gitlab api client: %w", err)
	}
	return nil
}

func (a *App) Run() error {
	containers, err := a.loadContainerList(a.ProjectID, a.ContainersListFile)
	if err != nil {
		return fmt.Errorf("failed to load %q: %w", a.ContainersListFile, err)
	}

	releaseTag := a.Release
	if releaseTag == "" {
		releaseTag, err = a.getLatestReleaseTag(a.ProjectID)
		if err != nil {
			return fmt.Errorf("failed to determine release tag for project %q: %w", a.ProjectID, err)
		}
	}
	fmt.Printf("using release tag %q\n", releaseTag)

	containerURIs, err := a.generateContainerURIs(a.ProjectID, releaseTag, containers)
	if err != nil {
		return fmt.Errorf("failed to generate container URIs for project %q: %w", a.ProjectID, err)
	}

	fmt.Printf("the following containers will be scanned:\n\t%s\n", strings.Join(containerURIs, "\n\t"))

	if a.ListOnly {
		return nil
	}
	return a.scanContainers(a.ProjectID, containerURIs)

	//nolint:godox
	// TODO: process the scan result and report vulneravilites that should be fixed.
}
