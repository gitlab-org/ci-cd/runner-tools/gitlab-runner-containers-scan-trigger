package main

import (
	"fmt"
	"os"
	"path"
	"strings"

	"github.com/xanzy/go-gitlab"
	"golang.org/x/mod/semver"
	"gopkg.in/yaml.v2"
)

const (
	releaseTagStrategy = "@release"
	lastTagStrategy    = "@last"

	containerRegistryBase = "registry.gitlab.com"
	gitlabRunnerProjectID = "gitlab-org/gitlab-runner"
)

type (
	Container = string
)

func (a *App) loadContainerList(projectID, filename string) ([]Container, error) {
	containers := map[string][]Container{}

	bytes, err := os.ReadFile(filename)
	if err != nil {
		return nil, fmt.Errorf("failed to read file %q : %w", filename, err)
	}

	err = yaml.Unmarshal(bytes, &containers)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal file %q : %w", filename, err)
	}

	result, ok := containers[projectID]
	if !ok {
		return nil, fmt.Errorf("no such project %q in containers list file", a.ProjectID)
	}

	return result, nil
}

func (a *App) getLatestReleaseTag(projectID string) (string, error) {
	gitTags, _, err := a.api.Tags.ListTags(projectID, &gitlab.ListTagsOptions{
		Sort:    gitlab.String("desc"),
		OrderBy: gitlab.String("version"),
	})
	if err != nil {
		return "", fmt.Errorf("failed to list git tags for project %q: %w", projectID, err)
	}

	if len(gitTags) == 0 {
		fmt.Printf("Project %q has no git tags; falling back to using gitlab-runner project git tags\n", projectID)
		return a.getLatestReleaseTag(gitlabRunnerProjectID)
	}

	releases := make([]string, 0, len(gitTags))
	for i := range gitTags {
		releases = append(releases, gitTags[i].Name)
	}
	semver.Sort(releases)
	return releases[len(releases)-1], nil
}

func (a *App) generateContainerURIs(projectID, releaseTag string, containers []Container) ([]string, error) {
	containerURIs := make([]string, 0, len(containers))
	for _, container := range containers {
		containerURI, err := a.generateContainerURI(projectID, container, releaseTag)
		if err != nil {
			return nil, fmt.Errorf("failed to generate URI for container %q: %w", container, err)
		}
		containerURIs = append(containerURIs, containerURI)
	}
	return containerURIs, nil
}

func (a *App) generateContainerURI(projectID, container, releaseTag string) (string, error) {
	switch {
	case strings.Contains(container, releaseTagStrategy):
		return a.interpolateReleaseTag(container, releaseTag)
	case strings.Contains(container, lastTagStrategy):
		return a.interpolateLastTag(projectID, container)
	default:
		return a.interpolateContainerURI(container, "", ""), nil
	}
}

func (a *App) interpolateContainerURI(container, token, tag string) string {
	return path.Join(containerRegistryBase, strings.ReplaceAll(container, token, tag))
}

func (a *App) interpolateReleaseTag(container, releaseTag string) (string, error) {
	return a.interpolateContainerURI(container, releaseTagStrategy, releaseTag), nil
}

func (a *App) interpolateLastTag(projectID, container string) (string, error) {
	lastTag, err := a.getLastTagForContainer(projectID, container)
	if err != nil {
		return "", err
	}
	return a.interpolateContainerURI(container, lastTagStrategy, lastTag), nil
}

func (a *App) getLastTagForContainer(projectID, containerName string) (string, error) {
	containerName = strings.Split(containerName, ":")[0]
	containerID, err := a.getContainerIDFromName(projectID, containerName)
	if err != nil {
		return "", fmt.Errorf("failed to determine last tag for %q container: %w", containerName, err)
	}

	repo, _, err := a.api.ContainerRegistry.GetSingleRegistryRepository(containerID, &gitlab.GetSingleRegistryRepositoryOptions{
		Tags: gitlab.Bool(true),
	})
	if err != nil {
		return "", fmt.Errorf("failed to get repository info for container %q: %w", containerName, err)
	}

	if len(repo.Tags) == 0 {
		return "", fmt.Errorf("no tags found for container %q", containerName)
	}

	return repo.Tags[len(repo.Tags)-1].Name, nil
}

func (a *App) getContainerIDFromName(projectID, containerName string) (int, error) {
	repos, _, err := a.api.ContainerRegistry.ListProjectRegistryRepositories(projectID, nil)
	if err != nil {
		return -1, fmt.Errorf("failed to list containers for project %q: %w", projectID, err)
	}
	for _, r := range repos {
		if r.Path == containerName {
			return r.ID, nil
		}
	}
	return -1, fmt.Errorf("failed to find container with name %q in project %q", containerName, projectID)
}
